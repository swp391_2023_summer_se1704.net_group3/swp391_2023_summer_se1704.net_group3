
USE master
GO

if exists (select * from sysdatabases where name='HomeSharingSystem') drop database HomeSharingSystem
GO

CREATE DATABASE HomeSharingSystem
GO

USE HomeSharingSystem
GO
---------------Create Table----------------

CREATE TABLE dbo.[UserRole](
	userRoleId int NOT NULL identity(1,1) PRIMARY KEY,
	userRoleName nvarchar(63) NOT NULL,
	[status] bit
);

CREATE TABLE dbo.[User](
	userId int NOT NULL identity(1,1) PRIMARY KEY,
	userName nvarchar(63) NOT NULL,
	[password] nvarchar(255) NOT NULL,
	roleId int NOT NULL,
	profilePic nvarchar(255),
	userMail nvarchar(255) UNIQUE,
	gender bit,
	userPhone nchar(10) UNIQUE,
	[status] bit,
	verificationStatus bit,
	FOREIGN KEY (roleID) REFERENCES dbo.[UserRole](userRoleId)
);

CREATE TABLE House (
    houseId int NOT NULL identity(1,1) PRIMARY KEY,
	houseName nvarchar(255) NOT NULL,
	area int NOT NULL,
	[location] nvarchar(255) NOT NULL,
	price int NOT NULL,
	toilet int,
	bedroom int,
	houseNumber int,
	flatNumber int,
	userId int NOT NULL 

);

CREATE TABLE Booking (
    bookingId INT PRIMARY KEY,
	userId INT NOT NULL,
	houseId INT NOT NULL,
    checkIn DATETIME NOT NULL,
    checkOut DATETIME NOT NULL,
    totalPrice NVARCHAR(255) NOT NULL,
	FOREIGN KEY (userID) REFERENCES [User] (userID),
	FOREIGN KEY (houseID) REFERENCES House (houseID)
);


CREATE TABLE Notification (
    notificationId INT PRIMARY KEY,
	userId INT NOT NULL,
	notificationType NVARCHAR(255) NOT NULL,
    [message] NVARCHAR(255),
    createDate DATETIME NOT NULL,
    FOREIGN KEY (userID) REFERENCES [User] (userID)
);

CREATE TABLE Bill (
    billID INT PRIMARY KEY,
	userID INT NOT NULL,
	houseID INT NOT NULL,
    quantity NVARCHAR(50) NOT NULL,
    [date] DATE NOT NULL,
	amount NVARCHAR(255) NOT NULL,
	note NVARCHAR(255) ,
    FOREIGN KEY (userID) REFERENCES [User] (userID),
	FOREIGN KEY (houseID) REFERENCES House (houseID)
);

CREATE TABLE Review (
    reviewId INT PRIMARY KEY,
	userId INT NOT NULL,
	houseId INT NOT NULL,
	rating NVARCHAR(50) NOT NULL,
    comment NVARCHAR(255) ,
    createDate DATE NOT NULL,
    FOREIGN KEY (userID) REFERENCES [User] (userID),
	FOREIGN KEY (houseID) REFERENCES House (houseID)
);

CREATE TABLE Post (
  postId INT PRIMARY KEY,
  houseId int not null,
  userID INT NOT NULL,
  title NVARCHAR(255),
  content NVARCHAR(255),
  created DATETIME NOT NULL,
  updated DATETIME NOT NULL,
  FOREIGN KEY (userID) REFERENCES [User] (userID),
  FOREIGN KEY (houseID) REFERENCES House (houseID)
);
CREATE TABLE Chat (
  chatID INT PRIMARY KEY,
  userID INT NOT NULL,
  sender NVARCHAR(255),
  receiver NVARCHAR(255),
  [message] NVARCHAR(255),
  [time] DATE NOT NULL,
  FOREIGN KEY (userID) REFERENCES [User] (userID)
);
-------------------------------------------
CREATE TABLE dbo.[Slider](
	sliderId	int				NOT NULL identity(1,1) PRIMARY KEY,
	sliderTitle nvarchar(255)	NOT NULL,
	[image]		nvarchar(255),
	[link]		int,
	note		nvarchar(255),
	[status]	bit,
	)
-----------------------------------------------------------
INSERT INTO dbo.UserRole(userRoleName,status) VALUES('Tenant',1);
INSERT INTO dbo.UserRole(userRoleName,status) VALUES('House Manager',1);
INSERT INTO dbo.UserRole(userRoleName,status) VALUES('Admin',1);
-------------User---------------
INSERT INTO dbo.[User](userName,[password],roleId,profilePic,userMail,gender,userPhone,[status], verificationStatus) 
				VALUES('TungBT',123456,1,'','tungbthe150621@fpt.edu.vn',1,'0696044711',1,1);
INSERT INTO dbo.[User](userName,[password],roleId,profilePic,userMail,gender,userPhone,[status], verificationStatus) 
				VALUES('Tenant',123456,1,'','tenant@gmail.com',1,'0696044712',1,1);
INSERT INTO dbo.[User](userName,[password],roleId,profilePic,userMail,gender,userPhone,[status],verificationStatus) 
				VALUES('HouseManager',123456,2,'','housemanager@gmail.com',1,'0696044713',1,1);
INSERT INTO dbo.[User](userName,[password],roleId,profilePic,userMail,gender,userPhone,[status], verificationStatus) 
				VALUES('Admin',123456,3,'','admin@gmail.com',1,'0696044714',1,1);
----------Slider---------------------
insert into Slider(sliderTitle,image,link,note,status) values ('House1','houseads1.jpg','1','House 1','1');
insert into Slider(sliderTitle,image,link,note,status) values ('House2','houseads2.png','2','House 2','1');
insert into Slider(sliderTitle,image,link,note,status) values ('House3','houseads3.png','3','House 3','1');
