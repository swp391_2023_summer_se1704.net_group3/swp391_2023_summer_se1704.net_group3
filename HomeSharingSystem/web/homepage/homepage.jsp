<%-- 
    Document   : homepage
    Created on : May 30, 2023, 8:24:55 PM
    Author     : Bùi Thanh Tùng
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Homepage</title>
        <!-- Google font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Audiowide">
        <!-- Boostraps5 -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrapp.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://kit.fontawesome.com/9650a62e47.js" crossorigin="anonymous"></script>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>    
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>


    </head>
    <style>
        .dropdown:hover>.dropdown-menu {
            display: block;
        }

        .dropdown>.dropdown-toggle:active {
            /*Without this, clicking will make it sticky*/
            pointer-events: none;
        }
        .navbar-collapse {
            z-index: 9999;
        }

    </style>
    <body>
        <!-- First navbar -->
        <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark" >
            <div class="container-fluid" >
                <a class="navbar-brand " href="/home" style="font-family:Audiowide, sans-serif; margin-left: 50px ">
                    <img src="/img/houselogo.png" width="50" height="50"  >
                    Home Sharing System
                </a>
            </div>
            <li class="nav-item me-2">
                <a class="nav-link active" aria-current="page" href="${contextPath}/login">
                    <font size="+2">Login/Register</font>
                </a>
            </li>
        </nav>
        <!-- Second navbar -->
        <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark ">
            <div class="mx-auto">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/home">
                            <font size="+2">Home</font>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">
                            <font size="+2">Post</font>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">
                            <font size="+2">Booking</font>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">
                            <font size="+2">Contact</font>

                        </a>
                    </li>


                </ul>
            </div>

            <!-- dropdown -->
            <ul class="navbar-nav" style="margin-right:100px">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <font size="+2">Menu</font></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="/user/profile.jsp">Profile</a></li>
                        <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#exampleModal" href="#">
                                About Us
                            </a>
                        </li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Log out</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- Modal about us -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Home Sharing System</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        SWP391_Group3
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <%--Slide starts here --%>	

            <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
                <%-- Indicators --%>  
                <%-- Wrapper for slides --%>
                <c:choose>
                    <%-- sliderList is empty --%>
                    <c:when test = "${empty sliderList}">
                        <div class="row">
                            <h5>No Slide available!</h5>
                        </div>
                    </c:when>
                    <%-- sliderList not empty --%>
                    <c:otherwise>
                        <div class="carousel-inner">
                            <div class="carousel-item active" data-bs-interval="1000">
                                <c:forEach items = "${sliderList}" var="slider" begin = "0" end = "0">
                                    <img style="width: 1500px; height: 800px; object-fit:cover" class="d-block w-100" src="img/${slider.getImage()}" alt="">
                                    <br>
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3>${slider.getNote()}</h3></div>
                                    </c:forEach>   
                            </div>

                            <c:forEach items = "${sliderList}" var="slider" begin = "1" end = "${sliderList.size()-1}">
                                <div class="carousel-item" data-bs-interval="1000">
                                    <img style="width: 1500px; height: 800px; object-fit:cover" class="d-block w-100" src="img/${slider.getImage()}" alt="">
                                    <br>
                                    <div class="carousel-caption d-none d-md-block">
                                        <h3>${slider.getNote()}</h3></div>
                                </div>
                            </c:forEach> 
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
            <h1>Latest Post</h1>
            <div class="card mb-3">
                <c:choose>
                    <%-- Post is empty --%>
                    <c:when test = "${empty sliderList}">
                        <div class="row">
                            <h5>No House available!</h5>
                        </div>
                    </c:when>
                    <c:otherwise>
                        
                            <c:forEach items = "${sliderList}" var="slider" begin = "0" end = "${sliderList.size()}">
                                <div class="row box-border" style="border: solid 1px">
                                    <div class="col-md-4">
                                        <img src="img/${slider.getImage()}" alt="house 1" class="img-fluid rounded-start" style="width:80%; height:200px; ">
                                    </div> 
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <h5 class="card-title">${slider.getNote()}</h5>   
                                            <a href="#" class="btn btn-primary card-text">Read More</a>
                                        </div>
                                    </div>
                               </div>
                            </c:forEach>
                    </div>
                    </c:otherwise>                
                </c:choose>
            </div>

            

            <div>
                <button class="btn btn-default" style="position: relative;left:45%;border:solid 2px;border-radius: 50px"><a href="${contextPath}/post">BROWSE ALL POST</a></button>  
            </div>

      














    </body>
</html>
