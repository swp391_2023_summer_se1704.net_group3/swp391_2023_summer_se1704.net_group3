<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    
<c:set var="dashboard" value="active"/>
<%@include file="../layout/layout.jsp" %>

<main>
    <ul class="box-info">
        <li>
            <i class='bx bxs-user' ></i>
            <span class="text">
                <h3>${requestScope.users}</h3>
                <p>Total Users</p>
            </span>
        </li>
        <li>
            <i class='bx bxl-blogger' ></i>
            <span class="text">
                <h3>${requestScope.blogs}</h3>
                <p>Total Post</p>
            </span>
        </li>
        <li>
            <i class='bx bxs-book' ></i>
            <span class="text">
                <h3>${requestScope.courses}</h3>
                <p>Total House</p>
            </span>
        </li>
    </ul>
</main>
</section>
</body>

