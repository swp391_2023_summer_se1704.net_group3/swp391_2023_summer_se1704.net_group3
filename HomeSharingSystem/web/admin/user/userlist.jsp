<%-- 
    Document   : userlist
    Created on : Dec 4, 2022, 11:47:24 AM
    Author     : hp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
 <c:set var="home" value="active"/>
<%@include file="../layout/layout.jsp" %>

<script>
    function doEdit(id)
    {
        window.location.href = "userdetail?id=" + id;
    }
    function change() {
        document.getElementById("f1").submit();
    }
</script>

<main>
   
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <header class="panel-heading">
                        <div class="panel-heading" style="display: flex;justify-content: space-between;align-items: center;">
                            <h1 style="padding: 10px"> User  List</h1>
                        </div>
                    </header>

                    <div class="panel-body table-responsive">
                        <div class="box-tools m-b-15" style="display: flex;">

                        </div>
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>UserName</th>
                                <th>Gender</th>
                                <th>Email</th>
                                <th>Moblie</th>
                                <th>Role</th>
                                <th>Status</th>
                            </tr>
                            <c:forEach items="${listUser}" var="i"  >
                                <tr>
                                    <td onclick="doEdit(${i.id})">${i.id}</td>
                                    <td onclick="doEdit(${i.id})">${i.username}</td>
                                    <td onclick="doEdit(${i.id})"> 
                                        ${i.gender eq true ? 'Male': 'Female'}

                                    </td>
                                    <td onclick="doEdit(${i.id})">${i.email}</td>
                                    <td onclick="doEdit(${i.id})">${i.phonenumber}</td>
                                    <td onclick="doEdit(${i.id})">${i.role.role_name}</td>
                                    <td>
                                        ${i.status eq true ? 'Deactive': 'Active'}
                                    </td>
                                </tr>

                            </c:forEach>  
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section>
</section>
</main> 