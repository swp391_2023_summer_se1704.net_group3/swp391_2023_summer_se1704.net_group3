<%-- 
    Document   : userdetail
    Created on : Dec 4, 2022, 12:59:26 PM
    Author     : hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../layout/layout.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <style>
            .container{
                padding: 10px;
                display: flex;
                flex-direction: column;
            }
            .input-container{
                display: flex;
                flex-direction: row;
                padding: 15px;
                justify-content: center;
            }
            .input{
                width: 50%;
                height: 30px;
                margin-left: 20px;
            }
            .input1{
                width: 21%;
            }
            p{
                width: 100px;
            }
            .button{
                width: 10%;
                margin: auto;

            }
        </style>
    </head>
    <body>
        <div >
            <form class="container" action="userdetail" method="Post">
                <input class="input" type="text" value="${user.id}" name="id" hidden="" />
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">UserName</label>
                    <div class="col-sm-10">
                        <input name ="username" type="text" class="form-control" value="${user.username}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Gender</label>
                    <div class="col-sm-10">
                        <input type="radio"  ${user.gender eq  true ?"checked":""} name="gender" value="true" required="required">
                        <label for="html">Male</label><br>
                        <input type="radio"  ${user.gender eq  false ?"checked":""} name="gender" value="false" required="required">
                        <label for="html">Female</label><br>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input name ="mail" type="text" class="form-control" value="${user.email}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Mobiphone</label>
                    <div class="col-sm-10">
                        <input name ="phone" type="text" class="form-control" value="${user.phonenumber}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"for="status">Status</label>
                    <div class="col-sm-10">
                        <select name="status" id="status">
                             <option value="false" ${user.status eq false ?"selected=\"selected\"" : ""}>Active</option>
                            <option value="true" ${user.status eq true?"selected=\"selected\"" : ""}>Deactive</option>
                        </select>
                    </div>
                </div>
                <input type="submit" value="Save" class="button" />
            </form>
        </div>

    </body>
</html>
