/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;




public class User {

    private int userId;
    private String userName;
    private String passWord;
    private int roleId;
    private String profilePic;
    private String userMail;
    private boolean gender;
    private String userPhone;
     private boolean status;
    private boolean verificationStatus;
    
    public User() {
        
    }

    public User(int userId, String userName, String passWord, int roleId, String profilePic, String userMail, boolean gender, String userPhone, boolean status, boolean verificationStatus) {
        this.userId = userId;
        this.userName = userName;
        this.passWord = passWord;
        this.roleId = roleId;
        this.profilePic = profilePic;
        this.userMail = userMail;
        this.gender = gender;
        this.userPhone = userPhone;
        this.status = status;
        this.verificationStatus = verificationStatus;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(boolean verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

    
}
