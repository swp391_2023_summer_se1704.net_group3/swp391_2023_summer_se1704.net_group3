package servlet;

import Bean.User;
import DAO.Impl.UserDAOImpl;
import common.GooglePojo;
import common.GoogleUtils;
import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/login-google")
public class LoginGoogleServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public LoginGoogleServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String code = request.getParameter("code");

        UserDAOImpl login = new UserDAOImpl();
        if (code == null || code.isEmpty()) {
            RequestDispatcher dis = request.getRequestDispatcher("login.jsp");
            dis.forward(request, response);
        } else {
            HttpSession session = request.getSession();
            String accessToken = GoogleUtils.getToken(code);
            GooglePojo googlePojo = GoogleUtils.getUserInfo(accessToken);
            User userRole = null;
            try {
                userRole = login.getUserByMail(googlePojo.getEmail());
            } catch (Exception ex) {
                Logger.getLogger(LoginGoogleServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            session.setAttribute("user", userRole);
            if (userRole == null) {
                response.sendRedirect(request.getContextPath() + "/login");
            } else {
                try {
                    int role = userRole.getRoleId();
                    switch (role) {
                        case 1:
                            response.sendRedirect(request.getContextPath() + "/admin");
                            break;
                        case 4:
                            request.getRequestDispatcher(request.getContextPath() + "/home").forward(request, response);
                            break;
                    }
                } catch (Exception e) {
                    response.sendRedirect(request.getContextPath() + "/login");

                }
            }
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
