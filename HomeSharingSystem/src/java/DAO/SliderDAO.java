/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Bean.Slider;
import java.util.ArrayList;

/**
 * Lớp này chứa các interface của SliderDAOImpl
 * @author Bùi Thanh Tùng
 */
public interface SliderDAO {
     /**
     * get all slider in database
     *
     * @return <code>ArrayList<Slider><code> object
     * @throws java.lang.Exception
     */
     public ArrayList<Slider> getSlider() throws Exception;
}
