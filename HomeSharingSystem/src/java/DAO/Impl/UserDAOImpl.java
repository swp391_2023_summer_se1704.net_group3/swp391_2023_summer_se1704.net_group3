/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO.Impl;

import Context.DBContext;
import DAO.UserDAO;
import Bean.User;
import Bean.UserRole;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAOImpl extends DBContext implements UserDAO {

    /**
     * Get all user
     *
     * @return
     * @throws Exception
     */
    @Override
    public ArrayList<User> getUserAllUser() throws Exception {
        ArrayList<User> newUserList = new ArrayList();
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "SELECT * FROM [User]";
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            rs = pre.executeQuery();
            while (rs.next()) {
                newUserList.add(new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verficationStatus")));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return newUserList;
    }

    /**
     * get new users
     *
     * @return <code>ArrayList<Use>r</code> object.
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<User> get10NewUser() throws Exception {
        ArrayList<User> newUserList = new ArrayList();
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "SELECT TOP 10 * FROM [User] ORDER BY userId DESC";
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            rs = pre.executeQuery();
            while (rs.next()) {
                newUserList.add(new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verficationStatus")));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return newUserList;
    }

    /**
     * get user from User table Using mail and password
     *
     * @param userMail is an String
     * @param password is an String
     * @return <code>User</code> object.
     * @throws java.lang.Exception
     */
    @Override
    public User getUserLogin(String userMail, String password) throws Exception {
        Connection conn = null;
        /* Result set returned by the sqlserver */
        ResultSet rs = null;
        /* Prepared statement for executing sql queries */
        PreparedStatement pre = null;

        String sql = "SELECT * FROM [User] WHERE userMail = ? and password = ?";
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setString(1, userMail);
            pre.setString(2, password);
            rs = pre.executeQuery();
            if (rs.next()) {
                User loginUser = new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verificationStatus"));
                return loginUser;
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return null;
    }

    /**
     * get user from User table using userId
     *
     * @param userId is an int
     * @return <code>User</code> object.
     * @throws java.lang.Exception
     */
    @Override
    public User getUserById(int userId) throws Exception {
        Connection conn = null;
        /* Result set returned by the sqlserver */
        ResultSet rs = null;
        /* Prepared statement for executing sql queries */
        PreparedStatement pre = null;

        String sql = "SELECT * FROM [User] WHERE userId = ?";
        User user = null;
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setInt(1, userId);
            rs = pre.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verificationStatus"));
                return user;
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return null;
    }

    /**
     * get user from User table using userMail
     *
     * @param userMail is an String
     * @return <code>User</code> object.
     */
    @Override
    public User getUserByMail(String userMail) throws Exception {
        Connection conn = null;
        /* Result set returned by the sqlserver */
        ResultSet rs = null;
        /* Prepared statement for executing sql queries */
        PreparedStatement pre = null;

        String sql = "SELECT * FROM [User] WHERE userMail = ?";
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setString(1, userMail);
            rs = pre.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verificationStatus"));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return null;
    }

    /**
     * get user from User table using userPhone
     *
     * @param userPhone is an String
     * @return <code>User</code> object.
     */
    @Override
    public User getUserByPhone(String userPhone) throws Exception {
        Connection conn = null;
        /* Result set returned by the sqlserver */
        ResultSet rs = null;
        /* Prepared statement for executing sql queries */
        PreparedStatement pre = null;
        String sql = "SELECT * FROM [User] WHERE userPhone = ?";
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setString(1, userPhone);
            rs = pre.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verificationStatus"));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return null;
    }

    /**
     * update a user from User table
     *
     * @param updatedUser is a <code>User</code> object
     * @return a int.
     */
    @Override
    public int updateUser(User updatedUser) throws Exception {
        Connection conn = null;
        /* Result set returned by the sqlserver */
        ResultSet rs = null;
        /* Prepared statement for executing sql queries */
        PreparedStatement pre = null;

        String sql = " UPDATE [User] set userName = ?, "
                + "[password] = ?,  "
                + "roleId = ?, "
                + "profilePic = ?, "
                + "userMail = ?, "
                + "gender = ?, "
                + "userPhone = ?, "
                + "status = ? "
                + "where userId = ?";
        int check = 0;
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setString(1, updatedUser.getUserName());
            pre.setString(2, updatedUser.getPassWord());
            pre.setInt(3, updatedUser.getRoleId());
            pre.setString(4, updatedUser.getProfilePic());
            pre.setString(5, updatedUser.getUserMail());
            pre.setBoolean(6, updatedUser.isGender());
            pre.setString(7, updatedUser.getUserPhone());
            pre.setBoolean(8, updatedUser.isStatus());
            pre.setInt(9, updatedUser.getUserId());
            check = pre.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }

        return check;
    }

    /**
     * change a user status from User table
     *
     * @param userId is an int
     * @param newStatus is a boolean object
     * @return a int.
     */
    @Override
    public int changeStatus(int userId, boolean newStatus) throws Exception {
        Connection conn = null;
        ResultSet rs = null;
        /* Result set returned by the sqlserver */
        PreparedStatement pre = null;
        /* Prepared statement for executing sql queries */

        String sql = "UPDATE [User] set [status] = ? where userId = ?";
        int check = 0;
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setBoolean(1, newStatus);
            pre.setInt(2, userId);
            check = pre.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return check;
    }

    /**
     * add a user to User table
     *
     * @param newUser is an <code>User</code> object
     * @return a int.
     */
    @Override
    public int addUser(User newUser) throws Exception {
        Connection conn = null;
        ResultSet rs = null;
        /* Result set returned by the sqlserver */
        PreparedStatement pre = null;
        /* Prepared statement for executing sql queries */

        String sql = "INSERT INTO [User](userName,[password],roleId,userMail,gender,userPhone,[status],verificationStatus)"
                + "values(?,?,?,?,?,?,?)";
        int check = 0;
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setString(1, newUser.getUserName());
            pre.setString(2, newUser.getPassWord());
            pre.setInt(3, 1);
            pre.setString(4, newUser.getUserMail());
            pre.setBoolean(5, newUser.isGender());
            pre.setString(6, newUser.getUserPhone());
            pre.setBoolean(7, false);
            pre.setBoolean(8, false);
            check = pre.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }

        return check;
    }

    /**
     * delete a user from User table
     *
     * @param user is an <code>User</code> object
     * @return a int.
     */
    @Override
    public int deleteUser(User user) throws Exception {
        Connection conn = null;
        ResultSet rs = null;
        /* Result set returned by the sqlserver */
        PreparedStatement pre = null;
        /* Prepared statement for executing sql queries */

        String sql = " delete from [User] where userId = ?";

        int check = 0;
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            pre.setInt(1, user.getUserId());
            check = pre.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return check;

    }

    @Override
    public int getTotalUser() {
        Connection conn = null;
        ResultSet rs = null;
        /* Result set returned by the sqlserver */
        PreparedStatement pre = null;
        /* Prepared statement for executing sql queries */

        String sql = "SELECT COUNT(*) AS total FROM User;";
        int check = 0;
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);

            while (rs.next()) {
                return rs.getInt("total");
            }
            check = pre.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return check;
    }

    @Override
    public HashMap<String, Integer> getUserCountByRole() throws Exception {
        HashMap<String, Integer> map = new HashMap();
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pre = null;
        String sql = "SELECT userRoleName, COUNT(userRoleId) AS number "
                + "FROM [UserRole] AS a JOIN [User] AS b ON a.userRoleId = b.roleId "
                + "GROUP BY a.userRoleId,a.userRoleName ";
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            rs = pre.executeQuery();
            while (rs.next()) {
                map.put(rs.getString("userRoleName"), rs.getInt("number"));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return map;
    }

    /**
     * Get all user regardless of status, based on criteria and sort in a
     * paginated form
     *
     * @param page
     * @param criteriaType
     * @param criteria
     * @param sortCriteria
     * @param sort
     * @return
     * @throws Exception
     */
    @Override
    public ArrayList<User> getTrueAllUserPaging(int page, String criteriaType, String criteria, String sortCriteria, String sort) throws Exception {
        ArrayList<User> newUserList = new ArrayList();
        /*Database Connection*/
        Connection conn = null;
        /*Result Set got from executing sql query*/
        ResultSet rs = null;
        /*Prepared Statement for executing query*/
        PreparedStatement pre = null;
        String queryCriteria = "";
        /*Set criteria based on the search type*/
        switch (criteriaType) {
            case "userName":
                queryCriteria = " WHERE userName LIKE ? ";
                criteria = criteria + "%";
                criteria = "%" + criteria;
                break;
            case "userMail":
                queryCriteria = " WHERE userMail LIKE ? ";
                criteria = criteria + "%";
                criteria = "%" + criteria;
                break;
            case "userPhone":
                queryCriteria = " WHERE userPhone = ? ";
                break;
            default:
                criteriaType = null;
                break;
        }
        switch (sortCriteria) {
            case "sortId":
                sort = " Order By userId " + sort;
                break;
            case "sortName":
                sort = " Order By userName " + sort;
                break;
            case "sortGender":
                sort = " Order By gender " + sort;
                break;
            case "sortMail":
                sort = " Order By userMail " + sort;
                break;
            case "sortMobile":
                sort = " Order By userPhone " + sort;
                break;
            case "sortRole":
                sort = " Order By roleId " + sort;
                break;
            case "sortStatus":
                sort = " Order By status " + sort;
                break;
            case "sortVeifyStatus":
                sort = " Order By vrificationStatus " + sort;
                break;
            default:
                sort = " Order By userId ASC";
        }

        /*Set page boundaries*/
        String pageBoundary = "";

        if (page > 0) {
            pageBoundary = "  WHERE A.num BETWEEN ? AND ?";
        }

        /*SQL query with the criteria*/
        String sql = "  SELECT * FROM (SELECT ROW_NUMBER()  OVER(" + sort + ") as num\n"
                + "				  ,[userId]\n"
                + "				  ,[userName]\n"
                + "				  ,[password]\n"
                + "				  ,[roleId]\n"
                + "				  ,[profilePic]\n"
                + "				  ,[userMail]\n"
                + "				  ,[gender]\n"
                + "				  ,[userPhone]\n"
                + "				  ,[status]\n"
                + "				  FROM [HomeSharingSystem].dbo.[User]\n"
                + queryCriteria + ") A\n" + pageBoundary;

        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            if (page > 0) {
                if (criteriaType != null) {
                    pre.setString(1, criteria);
                    pre.setInt(2, (page - 1) * 7 + 1);
                    pre.setInt(3, page * 7);
                } else if (page > 0) {
                    pre.setInt(1, (page - 1) * 7 + 1);
                    pre.setInt(2, page * 7);
                }
            } else if (criteriaType != null) {
                pre.setString(1, criteria);
            }

            rs = pre.executeQuery();
            while (rs.next()) {
                newUserList.add(new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verificationStatus")));

            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }

        return newUserList;
    }

    /**
     * Get filtered and sort Users paginated
     *
     * @param page
     * @param gender
     * @param role
     * @param status
     * @param sortCriteria
     * @param sort
     * @return
     * @throws Exception
     */
    @Override
    public ArrayList<User> getFilteredUserPaging(int page, int gender, int role, int status, String sortCriteria, String sort) throws Exception {
        ArrayList<User> newUserList = new ArrayList();
        /*Database Connection*/
        Connection conn = null;
        /*Result Set got from executing sql query*/
        ResultSet rs = null;
        /*Prepared Statement for executing query*/
        PreparedStatement pre = null;
        /*Set filter depends on the criteria*/
        String filter = "";
        if (gender != -1) {
            filter += " WHERE gender = ?";
            if (role != -1) {
                filter += " AND roleId = ?";
            }
            if (status != -1) {
                filter += " AND [status] = ?";
            }
        } else if (role != -1) {
            filter += "WHERE roleId = ?";
            if (status != -1) {
                filter += " AND [status] = ?";
            }
        } else if (status != -1) {
            filter += "WHERE [status] = ?";
        }
        /**
         * Sort
         */
        switch (sortCriteria) {
            case "sortId":
                sort = " Order By userId " + sort;
                break;
            case "sortName":
                sort = " Order By userName " + sort;
                break;
            case "sortGender":
                sort = " Order By gender " + sort;
                break;
            case "sortMail":
                sort = " Order By userMail " + sort;
                break;
            case "sortMobile":
                sort = " Order By userPhone " + sort;
                break;
            case "sortRole":
                sort = " Order By roleId " + sort;
                break;
            case "sortStatus":
                sort = " Order By status " + sort;
                break;
            default:
                sort = " Order By userId ASC";
        }

        /*Set page boundaries*/
        String pageBoundary = "";

        if (page > 0) {
            pageBoundary = "  WHERE A.num BETWEEN ? AND ?";
        }

        /*SQL query with the criteria*/
        String sql = "  SELECT * FROM (SELECT ROW_NUMBER()  OVER(" + sort + ") as num\n"
                + "				  ,[userId]\n"
                + "				  ,[userName]\n"
                + "				  ,[password]\n"
                + "				  ,[roleId]\n"
                + "				  ,[profilePic]\n"
                + "				  ,[userMail]\n"
                + "				  ,[gender]\n"
                + "				  ,[userPhone]\n"
                + "				  ,[status]\n"
                + "				  FROM [HomeSharingSystem].dbo.[User]\n"
                + filter + ") A\n" + pageBoundary;

        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            int parameterCount = 1;
            /*Set filter parrameter*/
            if (gender != -1) {
                pre.setInt(parameterCount++, gender);
            }
            if (role != -1) {
                pre.setInt(parameterCount++, role);
            }
            if (status != -1) {
                pre.setInt(parameterCount++, status);
            }
            /*Set paging parameter*/
            if (page > 0) {
                pre.setInt(parameterCount++, (page - 1) * 7 + 1);
                pre.setInt(parameterCount, page * 7);
            }

            rs = pre.executeQuery();
            while (rs.next()) {
                newUserList.add(new User(rs.getInt("userId"),
                        rs.getString("userName"),
                        rs.getString("password"),
                        rs.getInt("roleId"),
                        rs.getString("profilePic"),
                        rs.getString("userMail"),
                        rs.getBoolean("gender"),
                        rs.getString("userPhone"),
                        rs.getBoolean("status"),
                        rs.getBoolean("verificationStatus")));
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            closeResultSet(rs);
            closePreparedStatement(pre);
            closeConnection(conn);
        }
        return newUserList;
    }

//    public int getTotalAccount() {
//        try {
//            String sql = "SELECT COUNT(*) AS Total FROM Users";
//            PreparedStatement stm = connection.prepareCall(sql);
//            ResultSet rs = stm.executeQuery();
//            if (rs.next()) {
//                return rs.getInt("Total");
//            }
//            dbContext.closeConnection(connection, stm, rs);
//        } catch (SQLException ex) {
//            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return 0;
//    }
//    @Override
//    public User get(int id) {
//        try {
//            String sql = "SELECT * FROM Users u join UserRole ur\n"
//                    + "on u.roleId = ur.userRoleId where u.userId = ? ";
//            Connection connection = dbContext.getConnection();
//
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setInt(1, id);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                User u = new User();
//                u.setId(rs.getInt("userId"));
//                u.setUsername(rs.getString("userName"));
//                u.setPassword(rs.getString("password"));
//                u.setGender(rs.getBoolean("gender"));
//                u.setEmail(rs.getString("userMail"));
//                u.setPhonenumber(rs.getString("userPhone"));
//                u.setStatus(rs.getBoolean("status"));
//                u.setVeridication(rs.getBoolean("verificationStatus"));
//                UserRole r = new UserRole();
//                r.setRole_id(rs.getInt("roleId"));
//                r.setRole_name(rs.getString("userRoleName"));
//                r.setStatus(rs.getBoolean("status"));
//                u.setRole(r);
//                u.setProfilePic(rs.getString("profilePic"));
//                return u;
//            }
//        } catch (SQLException e) {
//            e.printStackTrace(System.out);
//        }
//        return null;
//    }
//    public List<User> getAll() {
//        List<User> list = new ArrayList<>();
//
//        try {
//            String sql = "SELECT * FROM Users u join UserRole ur\n"
//                    + "on u.roleId = ur.userRoleId ";
//            Connection connection = dbContext.getConnection();
//            PreparedStatement stm = connection.prepareStatement(sql);
//            ResultSet rs = stm.executeQuery();
//            while (rs.next()) {
//                User u = new User();
//                u.setId(rs.getInt("userId"));
//                u.setUsername(rs.getString("userName"));
//                u.setPassword(rs.getString("password"));
//                u.setGender(rs.getBoolean("gender"));
//                u.setEmail(rs.getString("userMail"));
//                u.setPhonenumber(rs.getString("userPhone"));
//                u.setStatus(rs.getBoolean("status"));
//                u.setVeridication(rs.getBoolean("verificationStatus"));
//                UserRole r = new UserRole();
//                r.setRole_id(rs.getInt("userRoleId"));
//                r.setRole_name(rs.getString("userRoleName"));
//                r.setStatus(rs.getBoolean("status"));
//                u.setRole(r);
//                u.setProfilePic(rs.getString("profilePic"));
//                list.add(u);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return list;
//    }
//
//    public int updateProfileUser(int id, String username, Boolean gender, String phone, Boolean status, String mail) {
//        String sql = "UPDATE Users\n"
//                + "SET\n"
//                + "userName = ?,\n"
//                + "userMail = ?,\n"
//                + "gender = ?,\n"
//                + "userPhone = ?,\n"
//                + "status = ?\n"
//                + "WHERE userId = ?;";
//        try {
//            Connection connection = dbContext.getConnection();
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setString(1, username);
//            ps.setString(2, mail);
//            ps.setBoolean(3, gender);
//            ps.setString(4, phone);
//            ps.setBoolean(5, status);
//            ps.setInt(6, id);
//            return ps.executeUpdate();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
//            System.out.println(ex.getMessage() + "loi o day");
//        }
//        return 0;
//    }
}
