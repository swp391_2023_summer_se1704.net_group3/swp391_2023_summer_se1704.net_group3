/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO.Impl;

import Context.DBContext;
import DAO.SliderDAO;
import Bean.Slider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Bùi Thanh Tùng
 */
public class SliderDAOImpl extends DBContext implements SliderDAO {
    
    @Override
    public ArrayList<Slider> getSlider() throws Exception {
        Connection conn = null;
        ResultSet rs = null;
        /* Result set returned by the sqlserver */
        PreparedStatement pre = null;
        /* Prepared statement for executing sql queries */

        ArrayList<Slider> allSlider = new ArrayList();
        String sql = "SELECT * FROM [Slider]";
        /* Get the slider */
        try {
            conn = getConnection();
            pre = conn.prepareStatement(sql);
            rs = pre.executeQuery();
            while (rs.next()) {
                int sliderId = rs.getInt("sliderId");
                String sliderTitle = rs.getString("sliderTitle");
                String image = rs.getString("image");
                String link = rs.getString("link");
                String note = rs.getString("note");
                allSlider.add(new Slider(sliderId, sliderTitle, image, link, note, true));
            }
        } catch (SQLException ex) {
            throw ex;
        }
        return allSlider;
    }
//    public static void main(String[] args) throws Exception {
//        SliderDAOImpl dao = new SliderDAOImpl();
//        for (Slider slider : dao.getSlider()) {
//            System.out.println(slider.getSliderTitle());
//        }
//    }
    
}
